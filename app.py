import os
import random
from flask import Flask
from flask import request
from flask import render_template
from flask import redirect
from flask import url_for
from flask import redirect
from flask import flash
from get_time import time_data
from flask import abort
from werkzeug.utils import secure_filename
import sqlite3
app = Flask(__name__)
app.debag = True

conn = sqlite3.connect('thread.db', check_same_thread=False)
cursor = conn.cursor()
cursor.execute('CREATE TABLE IF NOT EXISTS thread(id integer PRIMARY KEY, data text, post_text text, image_path text)')


@app.route("/")
def index():
    cursor.execute('SELECT * FROM thread')
    posts = cursor.fetchall()

    posts.reverse()
    return render_template('index.html', my_list=posts)


@app.route('/', methods=['POST', 'GET'])
def add_thread():
    filename = ''
    if request.method == 'POST':
        text_post = request.form["thread"]
        file = request.files['file']
        if file:
                filename = str(random.randint(1000000, 100000000000)) + secure_filename(file.filename)

                file.save(os.path.join('static/user_images', filename))

        if len(text_post) > 10000:
            flash('Слишком большой пост :(')
            return redirect(url_for('index'))
        if text_post != "":

            cursor.execute('INSERT INTO thread(data,post_text,image_path) VALUES(?,?,?)', (time_data(), text_post, filename,))
            conn.commit()
            cursor.execute('SELECT * FROM thread ORDER BY id DESC LIMIT 1;')
            number_last_post = cursor.fetchone()[0]
            last_post = 'post' + str(number_last_post)
            cursor.execute('SELECT Count(*) FROM thread')

            count_post = cursor.fetchone()[0]

            if count_post > 50:
                cursor.execute('SELECT id FROM thread')
                delete_post = cursor.fetchone()[0]
                delete_post_sql = 'DROP TABLE IF EXISTS post{}'.format(delete_post)
                cursor.execute(delete_post_sql)
                delete_file_sql = 'SELECT image_path FROM thread WHERE id = "{}"'.format(delete_post)
                cursor.execute(delete_file_sql)
                delete_file = cursor.fetchone()[0]
                if delete_file != "":
                    os.remove('./static/user_images/'+delete_file)

                delete_line_sql = 'DELETE FROM thread WHERE id = "{}"'.format(delete_post)
                cursor.execute(delete_line_sql)
                conn.commit()
            create_sql = 'CREATE TABLE IF NOT EXISTS {}(id integer PRIMARY KEY, data text, post_text text, image_path text)'.format(last_post)
            cursor.execute(create_sql)
            insert_sql = 'INSERT INTO {}(data,post_text,image_path) VALUES(?,?,?)'.format(last_post)
            cursor.execute(insert_sql, (time_data(), text_post, filename,))
            conn.commit()

    return redirect(url_for('index'))


@app.errorhandler(404)
def page_not_found(e):

    return render_template('404.html'), 404


@app.route('/post/<int:postID>')
def show_post(postID):
    cursor.execute('SELECT * FROM thread ORDER BY id DESC LIMIT 1;')
    number_last_post = cursor.fetchone()[0]
    if postID > number_last_post:
        return abort(404)
    select_sql = 'SELECT * FROM post{}'.format(postID)
    cursor.execute(select_sql)
    posts = cursor.fetchall()
    print(posts)
    return render_template('post.html', my_list=posts, postID=postID)


@app.route('/post/<int:postID>', methods=['POST', 'GET'])
def add_post(postID):
    filename = ''
    if request.method == 'POST':
        file = request.files['file']
        if file:
                filename = str(random.randint(1000000, 100000000000))+secure_filename(file.filename)

                file.save(os.path.join('static/user_images', filename))
        text_post = request.form["thread"]
        # print(request.environ.get('HTTP_X_REAL_IP', request.remote_addr))
        if len(text_post) > 10000:
            flash('Слишком большой пост :(')
            return redirect(url_for('show_post', postID=postID))
        if text_post != "":
            message_post = 'INSERT INTO post{}(data,post_text,image_path) VALUES(?,?,?)'.format(postID)
            cursor.execute(message_post, (time_data(), text_post, filename))
    return redirect(url_for('show_post', postID=postID))


if __name__ == "__main__":
    app.secret_key = '535378835'

    app.run(debug=True)
