import datetime


def time_data():
    now = datetime.datetime.now()
    now = now.strftime("%d-%m-%Y %H:%M:%S")
    return now
